# ShortLink App 

With help of this resourse users can create personal short links, save and track statistic of link's using.


## About

This project was created with [Create React App](https://github.com/facebook/create-react-app), saving information based on using [Redux] (https://redux.js.org/introduction/getting-started).
<p>It includes four pages with simple design created on SCSS:</p>
<p>-main page contening field for input links and creating short link; link's table with lazy pagination, there are user's links and information about stored;</p>
<p>-login page contenting fields for input user's login and password necessary for authorization on this resourse;</p>
<p>-registration page for creating account on this resourse enabling using it;</p>
<p>-notFound page.</p>
<p>Routing implemented with [React Router DOM] (https://www.npmjs.com/package/react-router-dom).</p>
<p>Documentationon the implementation of the main functionality is available at the link (http://79.143.31.216/docs).</p>


## Project setup

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.



