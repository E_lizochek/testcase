import "./Button.scss"
import classNames from 'classnames'

type Props = {
    name?: string;
    theme: 'dark' | 'light';
    type: "button" | "submit" | "reset";
    onClick?: () => void;
    title: string;
}
export function Button(props: Props): JSX.Element {

    const ButtonClass = classNames(
        'button',
        { 'button__dark': props.theme === 'dark' },
        { 'button__light': props.theme === 'light' })

    return (
        <button
            className={ButtonClass}
            name={props.name}
            type={props.type}
            onClick={props.onClick}
        >
            {props.title}
        </button>
    )
}