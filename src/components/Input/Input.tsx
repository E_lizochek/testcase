import React from 'react';
import './Input.scss'

type Props = {
    name?: string;
    type: 'text' | 'password';
    placeholder?: string;
    onChange: (event: string) => void;
}

export function Input(props: Props): JSX.Element {

    function handleChange(event: React.ChangeEvent<HTMLInputElement>): void {
        props.onChange(event.currentTarget.value)
    }

    return (
        <input className='input'
            name={props.name}
            type={props.type}
            placeholder={props.placeholder}
            onChange={handleChange} />
    )
}