import './RegistrationPage.scss'
import { Input, Button } from '../../components'
import { useState } from 'react'
import { useDispatch } from 'react-redux'
import { getRegistration } from '../../redux/actions'
import { Link } from 'react-router-dom'
import { useNavigate } from 'react-router-dom'

export function RegistrationPage() {
    const dispatch = useDispatch()
    const navigate = useNavigate()

    const [login, setLogin] = useState('')
    const [password, setPassword] = useState('')

    function handleLogin(value: string) {
        setLogin(value)
    }

    function handlePassword(value: string) {
        setPassword(value)
    }

    function handleSubmit() {
        dispatch(getRegistration({
            username: login,
            password: password
        }))
        navigate('/login', { replace: false })
    }

    return (
        <div className="registrationpage">
            <h1>Регистрация</h1>
            <Input
                name="Логин"
                type='text'
                placeholder='Введите логин'
                onChange={handleLogin}
            />
            <Input
                name="Придумайте пароль"
                type='password'
                placeholder='Введите пароль'
                onChange={handlePassword}
            />
            <div className="rbuttonbox">
                <Button
                    theme='dark'
                    type='button'
                    title="Зарегистрироваться"
                    onClick={handleSubmit} />
                <Link to='/login'> Уже есть аккаунт? Жми </Link>
            </div>
        </div>
    )
}