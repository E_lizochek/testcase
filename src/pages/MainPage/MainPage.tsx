import { useEffect } from "react";
import "./MainPage.scss"
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from '../../redux/store';
import { DataState } from '../../redux/reducer'
import { getShort, getDataStatistic } from "../../redux/actions";
import { Loader } from '../../components'
import { ShortLink } from "./components/ShortLink/ShortLink";
import { LinkTable } from "./components/LinkTable/LinkTable"

export function MainPage() {
    const state = useSelector<RootState, DataState>((state) => state)

    const dispatch = useDispatch()

    function handleDataStatistic() {
        dispatch(getDataStatistic({
            offset: state.offset,
            limit: state.limit
        }, state.token))
    }

    function handleShortLink(link) {
        dispatch(getShort(link, state.token))
        handleDataStatistic()
    }

    useEffect(() => {
        if (state.token !== null) {
            handleDataStatistic()
        }
    }, [])

    if (state.statistic === null) {
        return (<Loader />)
    }

    return (
        <div className="container">
            <ShortLink onSubmit={handleShortLink} shortLink={state.data} />
            <LinkTable data={state.statistic} />
        </div>
    )
}