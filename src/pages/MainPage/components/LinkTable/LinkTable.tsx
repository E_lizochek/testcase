import './LinkTable.scss'
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from '../../../../redux/store';
import { DataState } from '../../../../redux/reducer'
import { getDataStatistic } from "../../../../redux/actions";
import { Button } from "../../../../components";
import {LinkData} from '../../../../redux/reducer'

type Props = {
    data: LinkData[],
    title?: string,
}

const PAGINATION_NUMBER = 5;
const GENERAL_LINK = 'http://79.143.31.216/s/'

export function LinkTable(props: Props): JSX.Element {
    const { limit, offset, token } = useSelector<RootState, DataState>((state) => state)
    const dispatch = useDispatch()

    function calcLimitPagination() {
        return limit + PAGINATION_NUMBER
    }

    function handlePagination() {
        const newLimit = calcLimitPagination()
        dispatch(getDataStatistic({ offset: offset, limit: newLimit }, token))
    }

    return (
        <>
            <div className='table'>
                <div>Короткая ссылка</div>
                <div>Ссылка</div>
                <div>Количество переходов</div>
            </div>
            {props.data.map((item) => {
                return (
                    <div className='table' >
                        <a href={`${GENERAL_LINK + item.short}`} target="_blank">
                            {GENERAL_LINK + item.short}
                        </a>
                        <a className='table__target' href={`${item.target}`} target="_blank">
                            {item.target}
                        </a>
                        <div>{item.counter}</div>
                    </div>
                )
            })}
            <Button
                type='button'
                theme='light'
                title="Показать еще"
                onClick={handlePagination}
            />
        </>
    )
}