import "./ShortLink.scss"
import { useState } from "react";
import { Input, Button } from "../../../../components";
import { LinkData } from '../../../../redux/reducer'

type Props = {
    onSubmit: (link: string) => void;
    shortLink?: LinkData;
}

export function ShortLink(props: Props) {
    const [link, setLink] = useState('')

    function handleLink(value: string): void {
        setLink(value)
    }

    function submit(): void {
        props.onSubmit(link)
    }

    return (
        <div className="shortLink">
            <p>Введите ссылку</p>
            <Input
                type="text"
                onChange={handleLink}
            />
            <Button
                theme='dark'
                type='button'
                title="Сократить ссылку"
                onClick={submit}
            />
            <div className="shortLink__link">
                <div className="shortLink__short" id='copyText'>
                    Короткая ссылка: {props.shortLink ? (
                        <a href={`http://79.143.31.216/s/${props.shortLink.short}`} target="_blank">
                            http://79.143.31.216/s/{props.shortLink.short}
                        </a>)
                        : '...'}
                </div>
            </div>
        </div>
    )
}