import "./NotFound.scss"

type Props = {
    path?: string | undefined,
}

export function NotFound(props: Props): JSX.Element {
    return (
        <p className="notfound">
            404: Упс...Страница не найдена.</p>
    )
}