import { useState } from 'react'
import "./LoginPage.scss"
import { Input, Button } from "../../components"
import { getLogin } from '../../redux/actions'
import { useNavigate, Navigate } from 'react-router-dom'
import { Link } from "react-router-dom"
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from '../../redux/store';

export function LoginPage() {
    const token = useSelector<RootState>((state) => state.token)
    const [login, setLogin] = useState('')
    const [password, setPassword] = useState('')
    const dispatch = useDispatch()
    const navigate = useNavigate();

    function handleLogin(value: string) {
        setLogin(value)
    }

    function handlePassword(value: string) {
        setPassword(value)
    }

    function handleSubmit() {
        dispatch(getLogin({
            username: login,
            password: password,
        }))
    }

    console.log('token', token)
    if (token !== null) {
        return <Navigate to='/' />
    }

    return (
        <div className="loginpage">
            <h1>Вход</h1>
            <Input name='Логин'
                type='text'
                placeholder="Введите логин"
                onChange={handleLogin}
            />
            <Input name='Пароль'
                type='password'
                placeholder="Введите пароль"
                onChange={handlePassword}
            />
            <div className="buttonbox">
                <Button
                    theme="dark"
                    name="Логин"
                    type="submit"
                    title="Войти"
                    onClick={handleSubmit}
                />
                <Link to="/registration"> Нет аккаунта? Жми </Link>
            </div>
        </div>
    )
}