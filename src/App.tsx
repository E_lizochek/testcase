import React from 'react';
import './App.scss';
import { Provider } from 'react-redux';
import {AppRouting} from './router/AppRouting'
import { store } from './redux/store';



export default function App() {
  return (
    <div className="app">
      <Provider store={store}>
        <AppRouting/>
      </Provider>
    </div>
  );
}
