export async function getStatistic(body, token) {
    try {
        const response = await fetch(
            `http://79.143.31.216/statistics?offset=${body.offset}&limit=${body.limit}`,
            {
                method: 'GET',
                headers: {
                    Authorization: `Bearer ${token}`,
                    'Content-Type': 'application/json',
                },

            },
        )
        return response.json();
    } catch (error) {
        console.log('Error :', error);
    }
}