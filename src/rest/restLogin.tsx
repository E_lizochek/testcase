export async function fetchLogin(body: { username: string, password: string }) {
    try {
        const response = await fetch(
            `http://79.143.31.216/login`,
            {
                method: 'POST',
                body: `grant_type=&username=${body.username}&password=${body.password}&scope=&client_id=&client_secret=`,
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded",

                },
            },
        )

        return response.json();
    } catch (error) {
        console.log('Error :', error);
    }
}