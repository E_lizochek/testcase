export async function fetchShortLink(body, token) {
    try {
        const response = await fetch(
            `http://79.143.31.216/squeeze?link=${body}`,
            {
                method: 'POST',
                headers: {
                    Authorization: `Bearer ${token}`,
                    'Content-Type': 'application/json',
                },

            },
        )
        return response.json();
    } catch (error) {
        console.log('Error :', error);
    }
}