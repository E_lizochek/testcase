
export async function fetchRegistration(body: { username: string, password: string }) {
  try {
    const response = await fetch(
      `http://79.143.31.216/register?username=${body.username}&password=${body.password}`,
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
      },
    )
    return response.json();
  } catch (error) {
    console.log('Error :', error);
  }
}