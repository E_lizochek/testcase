
import {
    BrowserRouter as Router,
    Route,
    Routes,
} from 'react-router-dom';
import { LoginPage } from "../pages/LoginPage/LoginPage";
import { RegistrationPage } from "../pages/RegistrationPage/RegisrationPage"
import { NotFound } from '../pages/NotFoundPage/NotFound'
import { MainPage } from '../pages/MainPage/MainPage';
import { AuthRoute } from './AuthRoute';

export function AppRouting() {
    return (
        <Router>
            <Routes>
                <Route path="/" element={
                    <AuthRoute>
                        <MainPage />
                    </AuthRoute>
                } />
                <Route path="/login" element={<LoginPage />} />
                <Route path="/registration" element={<RegistrationPage />} />
                <Route path="*" element={<NotFound />} />
            </Routes>
        </Router>
    )
}