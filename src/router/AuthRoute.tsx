import { Navigate, useLocation } from "react-router-dom";
import { RootState } from '../redux/store';
import { DataState } from '../redux/reducer'
import { useSelector } from "react-redux";


export const AuthRoute = ({ children }: { children: JSX.Element }) => {
    const token = useSelector<RootState>((state) => state.token)
    const location = useLocation();

    if (token === null) {
        return <Navigate to="/login" state={{ from: location }} />;
    }
    return children;
};