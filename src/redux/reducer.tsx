import * as T from './actionTypes';


export type LinkData = {
    id: number;
    short: string;
    target: string;
    counter: boolean;
}

export type DataState = {
    token: string | null;
    user_id: string | null;
    data: LinkData;
    limit: number,
    offset: number,
    statistic: LinkData[];
}

const initialState: DataState = {
    token: null,
    user_id: null,
    data: null,
    limit: 5,
    offset: 0,
    statistic: null,
};

type Reducer = (
    state: DataState,
    action: any,
) => DataState;

export const rootReducer: Reducer = (
    state = initialState,
    { type, payload },
) => {
    switch (type) {
        case T.GET_USER_TOKEN: {
            return {
                ...state,
                token: payload,
            };
        }
        case T.GET_LINK: {
            return {
                ...state,
                data: payload,
            };
        }
        case T.GET_STATISTIC: {
            return {
                ...state,
                statistic: payload,
            };
        }
        case T.GET_LIMIT: {
            return {
                ...state,
                limit: payload,
            };
        }
        default: return state

    }
}