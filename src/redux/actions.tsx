import * as T from './actionTypes';
import { fetchRegistration } from '../rest/restRegistration'
import { fetchLogin } from '../rest/restLogin'
import { fetchShortLink } from '../rest/restShortLink'
import { getStatistic } from '../rest/getStatistic'
import { DataState } from './reducer'

export function getRegistration(body: { username: string, password: string }): any {
  return async function () {
    await fetchRegistration(body);
  };
}

export function getLogin(body: { username: string, password: string }): any {
  return async function (dispatch) {
    const data = await fetchLogin(body);
    dispatch(getToken(data.access_token))
  };
}

export function getShort(body: string, token: string): any {
  return async function (dispatch) {
    const data = await fetchShortLink(body, token);
    console.log(data)
    dispatch(getLink(data))
  }
}

export function getDataStatistic(body: { offset: number, limit: number }, token: any): any {
  return async function (dispatch) {
    const data = await getStatistic(body, token);
    dispatch(getStatisticData(data))
    dispatch(getLimit(body.limit))
  }
}

export function getToken(data: string) {
  return {
    type: T.GET_USER_TOKEN,
    payload: data
  }
}

export function getStatisticData(data: DataState[]) {
  return {
    type: T.GET_STATISTIC,
    payload: data
  }
}

export function getLink(data: string) {
  return {
    type: T.GET_LINK,
    payload: data
  }
}

export function getLimit(data: number) {
  return {
    type: T.GET_LIMIT,
    payload: data
  }
}
